//
//  SonosDiscover.h
//  Sonos Controller
//
//  Created by Axel Möller on 21/11/13.
//  Copyright (c) 2013 Appreviation AB. All rights reserved.
//
//  This code is distributed under the terms and conditions of the MIT license.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
import Foundation
import GCDAsyncUdpSocket.h
class SonosDiscover: NSObject, GCDAsyncUdpSocketDelegate {
    class func discoverControllers(completion: () -> Void) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {() -> Void in
            var discover: SonosDiscover = SonosDiscover()
            discover.findDevices({(ipAdresses: [AnyObject]) -> Void in
                var devices: [AnyObject] = [AnyObject]()
                if ipAdresses.count > 0 {
                    var ipAddress: String = ipAdresses[0]
                    var url: NSURL = NSURL(string: "http://\(ipAddress)/status/topology")!
                    var request: NSURLRequest = NSURLRequest(URL: url, cachePolicy: NSURLRequestReloadIgnoringCacheData, timeoutInterval: 5)
                    NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue(), completionHandler: {(response: NSURLResponse, data: NSData, error: NSError) -> Void in
                        var hResponse: NSHTTPURLResponse = (response as! NSHTTPURLResponse)
                        if hResponse.statusCode == 200 {
                            var responseDictionary: [NSObject : AnyObject] = XMLReader(dictionaryForXMLData: data!, error: error)
                            var inputDictionaryArray: [AnyObject] = responseDictionary["ZPSupportInfo"]["ZonePlayers"]["ZonePlayer"]
                            for dictionary: [NSObject : AnyObject] in inputDictionaryArray {
                                var name: String = dictionary["text"]
                                var coordinator: String = dictionary["coordinator"]
                                var uuid: String = dictionary["uuid"]
                                var group: String = dictionary["group"]
                                var ip: String = dictionary["location"].stringByReplacingOccurrencesOfString("http://", withString: "").stringByReplacingOccurrencesOfString("/xml/device_description.xml", withString: "")
                                var location: [AnyObject] = ip.componentsSeparatedByString(":")
                                var controllerObject: SonosController = SonosController(IP: location[0], port: CInt(location[1])!)
                                devices.append(["ip": location[0], "port": location[1], "name": name, "coordinator": Int((coordinator == "true") ? true : false), "uuid": uuid, "group": group, "controller": controllerObject])
                            }
                            var sort: NSSortDescriptor = NSSortDescriptor.sortDescriptorWithKey("name", ascending: true)
                            devices.sortUsingDescriptors([sort])
                            completion(devices, error)
                        }
                    })
                }
                else {
                    // No devices found
                    completion(nil, nil)
                }
            })
        })
    }

    func findDevices(block: findDevicesBlock) {
        self.completionBlock = block
        self.ipAddressesArray = NSArray()
        self.udpSocket = GCDAsyncUdpSocket(delegate: self, delegateQueue: dispatch_get_main_queue())
        var error: NSError? = nil
        if !self.udpSocket.bindToPort(0, error: error!) {
            NSLog("Error binding")
        }
        if !self.udpSocket.beginReceiving(error!) {
            NSLog("Error receiving")
        }
        self.udpSocket.enableBroadcast(TRUE, error: error!)
        if error! {
            NSLog("Error enabling broadcast")
        }
        var str: String = "M-SEARCH * HTTP/1.1\r\nHOST: 239.255.255.250:1900\r\nMAN: \"ssdp: discover\"\r\nMX: 3\r\nST: urn:schemas-upnp-org:device:ZonePlayer:1\r\n\r\n"
        self.udpSocket.sendData(str.dataUsingEncoding(NSUTF8StringEncoding), toHost: "239.255.255.250", port: 1900, withTimeout: -1, tag: 0)
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), {() -> Void in
            self.stopDiscovery()
        })
    }

    func stopDiscovery() {
        self.udpSocket.close()
        self.udpSocket = nil
        self.completionBlock(self.ipAddressesArray)
    }

    func udpSocket(sock: GCDAsyncUdpSocket, didReceiveData data: NSData, fromAddress address: NSData, withFilterContext filterContext: AnyObject) {
        var msg: String = String(data: data!, encoding: NSUTF8StringEncoding)
        if msg != "" {
            var reg: NSRegularExpression = NSRegularExpression(pattern: "http:\\/\\/(.*?)\\/", options: 0, error: nil)
            var matches: [AnyObject] = reg.matchesInString(msg, options: 0, range: NSMakeRange(0, msg.length))
            if matches.count > 0 {
                var result: NSTextCheckingResult = matches[0]
                var matched: String = msg.substringWithRange(result.rangeAtIndex(0))
                var ip: String = matched.substringFromIndex(7).substringToIndex(matched.length - 8)
                self.ipAddressesArray = self.ipAddressesArray.arrayByAddingObject(ip)
            }
        }
    }

    func findDevices(block: findDevicesBlock) {
    }

    func stopDiscovery() {
    }
    var udpSocket: GCDAsyncUdpSocket {
        get {
            var msg: String = String(data: data!, encoding: NSUTF8StringEncoding)
            if msg != "" {
                var reg: NSRegularExpression = NSRegularExpression(pattern: "http:\\/\\/(.*?)\\/", options: 0, error: nil)
                var matches: [AnyObject] = reg.matchesInString(msg, options: 0, range: NSMakeRange(0, msg.length))
                if matches.count > 0 {
                    var result: NSTextCheckingResult = matches[0]
                    var matched: String = msg.substringWithRange(result.rangeAtIndex(0))
                    var ip: String = matched.substringFromIndex(7).substringToIndex(matched.length - 8)
                    self.ipAddressesArray = self.ipAddressesArray.arrayByAddingObject(ip)
                }
            }
        }
    }

    var completionBlock: findDevicesBlock
    var ipAddressesArray: [AnyObject]
}
//
//  SonosDiscover.m
//  Sonos Controller
//
//  Created by Axel Möller on 21/11/13.
//  Copyright (c) 2013 Appreviation AB. All rights reserved.
//

typealias findDevicesBlock = (ipAddresses: [AnyObject]) -> Void