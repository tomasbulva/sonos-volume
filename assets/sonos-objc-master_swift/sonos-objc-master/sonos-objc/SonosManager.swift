//
//  SonosManager.h
//  Sonos Controller
//
//  Created by Axel Möller on 14/01/14.
//  Copyright (c) 2014 Appreviation AB. All rights reserved.
//
import Foundation
class SonosManager: NSObject {
    // Arrays containing all Sonos Devices on network
    var coordinators: [AnyObject]
    var slaves: [AnyObject]
    // The "current" device, ie. the device that acts if we select "play" now
    var currentDevice: SonosController

    class func sharedInstance() -> SonosManager {
        var sharedInstanceInstance: SonosManager? = nil
            var p: dispatch_once_t
        dispatch_once(p, {() -> Void in
            sharedInstanceInstance = self()
        })
        return sharedInstanceInstance!
    }
    // Returns a copy of all devices, coordinators + slaves

    func allDevices() -> [AnyObject] {
        return self.coordinators.arrayByAddingObjectsFromArray(self.slaves)
    }

    convenience override init() {
        self.init()
        if self != nil {
            self.coordinators = [AnyObject]()
            self.slaves = [AnyObject]()
            SonosDiscover.discoverControllers({(devices: [AnyObject], error: NSError) -> Void in
                self.willChangeValueForKey("allDevices")
                // Save all devices
                for device: [NSObject : AnyObject] in devices {
                    var controller: SonosController = SonosController(IP: device["ip"], port: CInt(device["port"])!)
                    controller.group = device["group"]
                    controller.name = device["name"]
                    controller.uuid = device["uuid"]
                    controller.coordinator = CBool(device["coordinator"])!
                    if controller.isCoordinator() {
                        self.coordinators.append(controller)
                    }
                    else {
                        self.slaves.append(controller)
                    }
                }
                // Add slaves to masters
                for slave: SonosController in self.slaves {
                    for coordinator: SonosController in self.coordinators {
                        if (coordinator.group() == slave.group()) {
                            coordinator.slaves().append(slave)
                        }
                    }
                }
                // Find current device (this implementation may change in the future)
                if self.allDevices().count > 0 {
                    self.willChangeValueForKey("currentDevice")
                    self.currentDevice = self.coordinators[0]
                    for controller: SonosController in self.coordinators {
                        // If a coordinator is playing, make it the current device
                        controller.playbackStatus({(playing: Bool, response: [NSObject : AnyObject], error: NSError) -> Void in
                            if playing {
                                self.currentDevice = controller
                            }
                        })
                    }
                    self.didChangeValueForKey("currentDevice")
                }
                self.didChangeValueForKey("allDevices")
            })
        }
    }
}
//
//  SonosManager.m
//  Sonos Controller
//
//  Created by Axel Möller on 14/01/14.
//  Copyright (c) 2014 Appreviation AB. All rights reserved.
//