//
//  SonosController.h
//  Sonos Controller
//
//  Created by Axel Möller on 16/11/13.
//  Copyright (c) 2013 Appreviation AB. All rights reserved.
//
//  This code is distributed under the terms and conditions of the MIT license.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
import Foundation
class SonosController: NSObject {
    var ip: String
    var port: Int
    var group: String
    var name: String
    var uuid: String
    var coordinator: Bool
    var slaves: [AnyObject]
    /**
     Creates and returns a Sonos Controller object.
     By default, the SOAP interface on Sonos Devices operate on port 1400, but use initWithIP:port: if you need to specify another port
     
     Use SonosManager to create these controllers automatically
     */

    convenience override init(IP ip_: String) {
        self = self(IP: ip_, port: 1400)
    }

    convenience override init(IP ip_: String, port port_: Int) {
        self.init()
        self.ip = ip_
        self.port = port_
        self.slaves = [AnyObject]()
    }
    /**
     All SOAP methods returns asynchronus XML data from the Sonos Device in an NSDictionary format for easy reading.
     Some methods returns quick values that are interesting, eg. getVolume:completion: returns an integer containing the volume,
     together with the entire XML response
     
     All methods returns a non-empty NSError object on failure
     */
    /**
     Plays a track
     
     @param track The Track URI, may be nil to just play current track
     @param block Objective-C block to call on finish
     */

    func play(track: String, completion block: (response: [NSObject : AnyObject], error: NSError) -> Void) {
        if track != nil {
            var meta: String = "<DIDL-Lite xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xmlns:upnp=\"urn:schemas-upnp-org:metadata-1-0/upnp/\" xmlns:r=\"urn:schemas-rinconnetworks-com:metadata-1-0/\" xmlns=\"urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/\"><item id=\"10000000spotify%3atrack%3a3bT5PDBhVj4ifU11zQvGP2\" restricted=\"true\">" < dc:
            title >  <  / dc:
            title >  < upnp:
            self > object.item.audioItem.musicTrack <  / upnp:
            self >  < desc
            id =
            "cdudn\" nameSpace=\"urn:schemas-rinconnetworks-com:metadata-1-0/\">SA_RINCON2311_X_#Svc2311-0-Token</desc>" <  / item >  <  / DIDL - Lite > ";
        
        [self
         upnp:@" / MediaRenderer / AVTransport / Control
            "
         soap_service:@"
                        schemas - upnp - org:
                                    1
            "
         soap_action:@" + 
                "
         soap_arguments:[NSString stringWithFormat:@" < InstanceID > 0 <  / InstanceID >  < CurrentURI >  % 
            CurrentURI >  < CurrentURIMetaData >  % 
            CurrentURIMetaData > ", track, meta]
         completion:^(id responseObject, NSError *error) {
             [self play:nil completion:block];
         }];
    } else {
        [self
         upnp:@" / MediaRenderer / AVTransport / Control
            "
         soap_service:@"
                        schemas - upnp - org:
                                    1
            "
         soap_action:@" + 
                "
         soap_arguments:@" < InstanceID > 0 <  / InstanceID >  < Speed > 1 <  / Speed > "
         completion:block];
    }
}

- (void)play:(NSString *)track URIMetaData:(NSString *)URIMetaData completion:(void (^)(NSDictionary *reponse, NSError *error))block {
    [self
     upnp:@" / MediaRenderer / AVTransport / Control
            "
     soap_service:@"
                        schemas - upnp - org:
                                    1
            "
     soap_action:@" + 
                "
     soap_arguments:[NSString stringWithFormat:@" < InstanceID > 0 <  / InstanceID >  < CurrentURI >  % 
            CurrentURI >  < CurrentURIMetaData >  % 
            CurrentURIMetaData > ", track, URIMetaData]
     completion:^(id responseObject, NSError *error) {
         [self play:nil completion:block];
     }];
}

- (void)playSpotifyTrack:(NSString *)track completion:(void (^)(NSDictionary *reponse, NSError *error))block {
    
    NSString *trackEncoded = [track stringByReplacingOccurrencesOfString:@" + 
                " withString:@" %  % 3
        }
    }
    /**
     Plays a track with custom URI Metadata, Spotify etc needs this, see playSpotifyTrack:completion:
     
     @param track The track URI
     @param URIMetaData Metadata XML String
     @param block Objective-C block to call on finish
     */

    func play(track: String, URIMetaData: String, completion block: (response: [NSObject : AnyObject], error: NSError) -> Void) {
    }
    /**
     Plays a Spotify track
     
     @param track Spotify track URI
     @param block Objective-C block to call on finish
     */

    func playSpotifyTrack(track: String, completion block: (response: [NSObject : AnyObject], error: NSError) -> Void) {
    }
    /**
     Play track in queue
     
     @param position. Starts counting at 1
     @param block Objective-C block to call on finish
     */

    func playQueuePosition(position: Int, completion block: (response: [NSObject : AnyObject], error: NSError) -> Void) {
    }
    /**
     Pause playback
     
     @param block Objective-C block to call on finish
     */

    func pause(block: (response: [NSObject : AnyObject], error: NSError) -> Void) {
    }
    /**
     Toggle playback, pause if playing, play if paused
     BOOL in block function contains current playback state
     
     @param block Objective-C block to call on finish
     */

    func togglePlayback(block: (playing: Bool, response: [NSObject : AnyObject], error: NSError) -> Void) {
    }
    /**
     Next track
     
     @param block Objective-C block to call on finish
     */

    func next(block: (response: [NSObject : AnyObject], error: NSError) -> Void) {
    }
    /**
     Previous track
     
     @param block Objective-C block to call on finish
     */

    func previous(block: (response: [NSObject : AnyObject], error: NSError) -> Void) {
    }
    /**
     Queue a track
     
     @param track The Track URI, may not be nil
     @param block Objective-C block to call on finish
     */

    func queue(track: String, replace: Bool, completion block: (response: [NSObject : AnyObject], error: NSError) -> Void) {
    }
    /**
     Queue a track with custom URI Metadata
     Needed for Spotify etc
     
     @param track The Track URI
     @param URIMetaData URI Metadata XML String
     @param block Objective-C block to call on finish
     */

    func queue(track: String, URIMetaData: String, replace: Bool, completion block: (response: [NSObject : AnyObject], error: NSError) -> Void) {
    }
    /*
     Queue a Spotify playlist
     eg. spotify:user:aaa:playlist:xxxxxxxxxx
     
     @param playlist The playlist URI
     @param replace Replace current queue
     @param block Objective-C block to call on finish
     */

    func queueSpotifyPlaylist(playlist: String, replace: Bool, completion block: (response: [NSObject : AnyObject], error: NSError) -> Void) {
    }
    /**
     Get current volume of device.
     This method returns an NSInteger with the volume level (0-100)
     
     @param block Objective-C block to call on finish
     */

    func getVolume(block: (volume: Int, response: [NSObject : AnyObject]) -> Void) {
    }
    /**
     Set volume of device
     
     @param volume The volume (0-100)
     @param block Objective-C block to call on finish
     */

    func setVolume(volume: Int, completion block: (response: [NSObject : AnyObject], error: NSError) -> Void) {
    }
    /**
     Get mute status
     
     @param block Objective-C block to call on finish
     */

    func getMute(block: (mute: Bool, response: [NSObject : AnyObject], error: NSError) -> Void) {
    }
    /**
     Set or unset mute on device
     
     @param mute Bool value
     @param block Objective-C block to call on finish
     */

    func setMute(mute: Bool, completion block: (response: [NSObject : AnyObject], error: NSError) -> Void) {
    }
    /**
     Get current track info.
     
     @param block Objective-C block to call on finish
     */

    func trackInfo(block: (artist: String, title: String, album: String, albumArt: NSURL, time: Int, duration: Int, queueIndex: Int, trackURI: String, protocol: String, error: NSError) -> Void) {
    }

    func mediaInfo(block: (response: [NSObject : AnyObject], error: NSError) -> Void) {
    }
    /**
     Playback status
     Returns playback boolean value in block
     
     @param block Objective-C block to call on finish
     */

    func playbackStatus(block: (playing: Bool, response: [NSObject : AnyObject], error: NSError) -> Void) {
    }
    /**
     More detailed version of playbackStatus:
     Get status info (playback status). The only interesting data IMO is CurrentTransportState that tells if the playback is active. Thus returns:
     - CurrentTransportState - {PLAYING|PAUSED_PLAYBACK|STOPPED}
     
     @param block Objective-C block to call on finish
     */

    func status(block: (response: [NSObject : AnyObject], error: NSError) -> Void) {
    }

    func browse(block: (response: [NSObject : AnyObject], error: NSError) -> Void) {
    }

    func upnp(url: String, soap_service: String, soap_action: String, soap_arguments: String, completion block: () -> Void) {
            // Create Body data
        var post_xml: NSMutableString = NSMutableString()
        post_xml.appendString("<s:Envelope xmlns:s='http://schemas.xmlsoap.org/soap/envelope/' s:encodingStyle='http://schemas.xmlsoap.org/soap/encoding/'>")
        post_xml.appendString("<s:Body>")
        post_xml.appendFormat("<u:%@ xmlns:u='%@'>", soap_action, soap_service)
        post_xml.appendString(soap_arguments)
        post_xml.appendFormat("</u:%@>", soap_action)
        post_xml.appendString("</s:Body>")
        post_xml.appendString("</s:Envelope>")
            // Create HTTP Request
        var request: NSMutableURLRequest = NSMutableURLRequest()
        request.URL = NSURL(string: "http://\(self.ip):\(self.port)\(url)")!
        request.HTTPMethod = "POST"
        request.timeoutInterval = 15.0
        // Set headers
        request.addValue("text/xml", forHTTPHeaderField: "Content-Type")
        request.addValue("\(soap_service)#\(soap_action)", forHTTPHeaderField: "SOAPACTION")
        // Set Body
        request.HTTPBody = post_xml.dataUsingEncoding(NSUTF8StringEncoding)
        var requestOperation: AFHTTPRequestOperation = AFHTTPRequestOperation(request: request)
        requestOperation.setCompletionBlockWithSuccess({(operation: AFHTTPRequestOperation, responseObject: AnyObject) -> Void in
            if block != nil {
                var response: [NSObject : AnyObject] = XMLReader(dictionaryForXMLData: responseObject!, error: nil)
                block(response, nil)
            }
        }, failure: {(operation: AFHTTPRequestOperation, error: NSError) -> Void in
            if block != nil {
                block(nil, error)
            }
        })
        requestOperation.start()
    }

    func upnp(url: String, soap_service: String, soap_action: String, soap_arguments: String, completion block: () -> Void) {
    }
}
//
//  SonosController.m
//  Sonos Controller
//
//  Created by Axel Möller on 16/11/13.
//  Copyright (c) 2013 Appreviation AB. All rights reserved.
//

import AFNetworking
    var amp: 

var id: item = 

var id: desc = 

    var lt: 

    var gt: 

func () {
    var playQueuePosition
    var completion: position
}

func block() {
    self.upnp("/MediaRenderer/AVTransport/Control", soap_service: "urn:schemas-upnp-org:service:AVTransport:1", soap_action: "Seek", soap_arguments: "<InstanceID>0</InstanceID><Unit>TRACK_NR</Unit><Target>\(position)</Target>", completion: {(response: [NSObject : AnyObject], error: NSError) -> Void in
        if error && block {
            block(response, error)
        }
        else {
            self.play(nil, completion: block)
        }
    })
}

func () {
    var pause
}

func block() {
    self.upnp("/MediaRenderer/AVTransport/Control", soap_service: "urn:schemas-upnp-org:service:AVTransport:1", soap_action: "Pause", soap_arguments: "<InstanceID>0</InstanceID><Speed>1</Speed>", completion: block)
}

func () {
    var togglePlayback
}

func block() {
    self.playbackStatus({(playing: Bool, response: [NSObject : AnyObject], error: NSError) -> Void in
        if playing {
            self.pause({(response: [NSObject : AnyObject], error: NSError) -> Void in
                if error != nil {
                    block(false, nil, error)
                }
                block(false, response, nil)
            })
        }
        else {
            self.play(nil, completion: {(response: [NSObject : AnyObject], error: NSError) -> Void in
                if error != nil {
                    block(false, nil, error)
                }
                block(true, response, error)
            })
        }
    })
}

func () {
    var next
}

func block() {
    self.upnp("/MediaRenderer/AVTransport/Control", soap_service: "urn:schemas-upnp-org:service:AVTransport:1", soap_action: "Next", soap_arguments: "<InstanceID>0</InstanceID><Speed>1</Speed>", completion: block)
}

func () {
    var previous
}

func block() {
    self.upnp("/MediaRenderer/AVTransport/Control", soap_service: "urn:schemas-upnp-org:service:AVTransport:1", soap_action: "Previous", soap_arguments: "<InstanceID>0</InstanceID><Speed>1</Speed>", completion: block)
}

func () {
    var clearQueue
}

func block() {
    self.upnp("/MediaRenderer/AVTransport/Control", soap_service: "urn:schemas-upnp-org:service:AVTransport:1", soap_action: "RemoveAllTracksFromQueue", soap_arguments: "<InstanceID>0</InstanceID>", completion: block)
}

func () {
    var queue
    var replace: track
    var completion: replace
}

func block() {
    if replace != nil {
        self.clearQueue({(response: [NSObject : AnyObject], error: NSError) -> Void in
            if error && block {
                block(response, error)
            }
            else {
                self.queue(track, replace: false, completion: block)
            }
        })
    }
    else {
        self.upnp("/MediaRenderer/AVTransport/Control", soap_service: "urn:schemas-upnp-org:service:AVTransport:1", soap_action: "AddURIToQueue", soap_arguments: "<InstanceID>0</InstanceID><EnqueuedURI>\(track)</EnqueuedURI><EnqueuedURIMetaData></EnqueuedURIMetaData><DesiredFirstTrackNumberEnqueued>0</DesiredFirstTrackNumberEnqueued><EnqueueAsNext>1</EnqueueAsNext>", completion: block)
    }
}

func () {
    var queue
    var URIMetaData: track
    var replace: URIMetaData
    var completion: replace
}

func block() {
    if replace != nil {
        self.clearQueue({(response: [NSObject : AnyObject], error: NSError) -> Void in
            if error && block {
                block(response, error)
            }
            else {
                self.queue(track, URIMetaData: URIMetaData, replace: false, completion: block)
            }
        })
    }
    else {
        self.upnp("/MediaRenderer/AVTransport/Control", soap_service: "urn:schemas-upnp-org:service:AVTransport:1", soap_action: "AddURIToQueue", soap_arguments: "<InstanceID>0</InstanceID><EnqueuedURI>\(track)</EnqueuedURI><EnqueuedURIMetaData>\(URIMetaData)</EnqueuedURIMetaData><DesiredFirstTrackNumberEnqueued>0</DesiredFirstTrackNumberEnqueued><EnqueueAsNext>1</EnqueueAsNext>", completion: block)
    }
}

func () {
    var queueSpotifyPlaylist
    var replace: playlist
    var completion: replace
}

func block() {
    var playlistURI: [AnyObject] = playlist.componentsSeparatedByString(":")
    var playlistOwner: String = playlistURI[2]
    var playlistID: String = playlistURI[4]
    var meta: String = 
    var stringWithFormat: String
    "<DIDL-Lite xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xmlns:upnp=\"urn:schemas-upnp-org:metadata-1-0/upnp/\" xmlns:r=\"urn:schemas-rinconnetworks-com:metadata-1-0/\" xmlns=\"urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/\"> " < item
    id =
    "10060a6cspotify%%3auser%%3a%@%%3aplaylist%%3a%@\" parentID=\"100a0664playlists\" restricted=\"true\"> " < upnp:
    self > object.container.playlistContainer <  / upnp:
    self >  < desc
    id =
    "cdudn\" nameSpace=\"urn:schemas-rinconnetworks-com:metadata-1-0/\">SA_RINCON2311_X_#Svc2311-0-Token</desc> " <  / item >  <  / DIDL - Lite > ", playlistOwner, playlistID];
    
    meta = [[[meta stringByReplacingOccurrencesOfString:@" < " withString:@" & lt
    "] stringByReplacingOccurrencesOfString:@" > " withString:@" & gt
    "] stringByReplacingOccurrencesOfString:@" + 
        ""
        "&quot;"
    if replace != nil {
        self.clearQueue({(response: [NSObject : AnyObject], error: NSError) -> Void in
            if error != nil {
                NSLog("Error clearing queue: %@", error.localizedDescription)
                if block != nil {
                    block(response, error)
                }
            }
            else {
                self.upnp("/MediaRenderer/AVTransport/Control", soap_service: "urn:schemas-upnp-org:service:AVTransport:1", soap_action: "AddURIToQueue", soap_arguments: "<InstanceID>0</InstanceID><EnqueuedURI>x-rincon-cpcontainer:10060a6cspotify%%3auser%%3a\(playlistOwner)%%3aplaylist%%3a\(playlistID)</EnqueuedURI><EnqueuedURIMetaData>\(meta)</EnqueuedURIMetaData><DesiredFirstTrackNumberEnqueued>0</DesiredFirstTrackNumberEnqueued><EnqueueAsNext>0</EnqueueAsNext>", completion: block)
            }
        })
    }
    else {
        self.upnp("/MediaRenderer/AVTransport/Control", soap_service: "urn:schemas-upnp-org:service:AVTransport:1", soap_action: "AddURIToQueue", soap_arguments: "<InstanceID>0</InstanceID><EnqueuedURI>x-rincon-cpcontainer:10060a6cspotify%%3auser%%3a\(playlistOwner)%%3aplaylist%%3a\(playlistID)</EnqueuedURI><EnqueuedURIMetaData>\(meta)</EnqueuedURIMetaData><DesiredFirstTrackNumberEnqueued>0</DesiredFirstTrackNumberEnqueued><EnqueueAsNext>0</EnqueueAsNext>", completion: block)
    }
}

func () {
    var getVolume
}

func block() {
    self.upnp("/MediaRenderer/RenderingControl/Control", soap_service: "urn:schemas-upnp-org:service:RenderingControl:1", soap_action: "GetVolume", soap_arguments: "<InstanceID>0</InstanceID><Channel>Master</Channel>", completion: {(response: [NSObject : AnyObject], error: NSError) -> Void in
        var value: String = response["s:Envelope"]["s:Body"]["u:GetVolumeResponse"]["CurrentVolume"]["text"]
        if (value == "") {
            block(0, response, error)
        }
        else {
            block(CInteger(value)!, response, nil)
        }
    })
}

func () {
    var setVolume
    var completion: volume
}

func block() {
    self.upnp("/MediaRenderer/RenderingControl/Control", soap_service: "urn:schemas-upnp-org:service:RenderingControl:1", soap_action: "SetVolume", soap_arguments: "<InstanceID>0</InstanceID><Channel>Master</Channel><DesiredVolume>\(volume)</DesiredVolume>", completion: block)
}

func () {
    var getMute
}

func block() {
    self.upnp("/MediaRenderer/RenderingControl/Control", soap_service: "urn:schemas-upnp-org:service:RenderingControl:1", soap_action: "GetMute", soap_arguments: "<InstanceID>0</InstanceID><Channel>Master</Channel>", completion: {(response: [NSObject : AnyObject], error: NSError) -> Void in
        if block != nil {
            if error != nil {
                block(false, response, error)
            }
            var stateStr: String = response["s:Envelope"]["s:Body"]["u:GetMuteResponse"]["CurrentMute"]["text"]
            var state: Bool = (stateStr == "1") ? TRUE : FALSE
            block(state, response, nil)
        }
    })
}

func () {
    var setMute
    var completion: mute
}

func block() {
    self.upnp("/MediaRenderer/RenderingControl/Control", soap_service: "urn:schemas-upnp-org:service:RenderingControl:1", soap_action: "SetMute", soap_arguments: "<InstanceID>0</InstanceID><Channel>Master</Channel><DesiredMute>\(mute)</DesiredMute>", completion: block)
}

func () {
    var trackInfo
}

func block() {
    self.upnp("/MediaRenderer/AVTransport/Control", soap_service: "urn:schemas-upnp-org:service:AVTransport:1", soap_action: "GetPositionInfo", soap_arguments: "<InstanceID>0</InstanceID>", completion: {(response: [NSObject : AnyObject], error: NSError) -> Void in
        if error != nil {
            block(nil, nil, nil, nil, 0, 0, 0, nil, nil, error)
        }
        var positionInfoResponse: [NSObject : AnyObject] = response["s:Envelope"]["s:Body"]["u:GetPositionInfoResponse"]
        var trackMetaData: [NSObject : AnyObject] = XMLReader(dictionaryForXMLString: positionInfoResponse["TrackMetaData"]["text"], error: nil)
            // Save track meta data
        var artist: String = trackMetaData["DIDL-Lite"]["item"]["dc:creator"]["text"]
        var title: String = trackMetaData["DIDL-Lite"]["item"]["dc:title"]["text"]
        var album: String = trackMetaData["DIDL-Lite"]["item"]["upnp:album"]["text"]
        var albumArt: NSURL = NSURL(string: "http://\(self.ip):\(self.port)\(trackMetaData["DIDL-Lite"]["item"]["upnp:albumArtURI"]["text"])")!
            // Convert current progress time to seconds
        var timeString: String = positionInfoResponse["RelTime"]["text"]
        var times: [AnyObject] = timeString.componentsSeparatedByString(":")
        var hours: Int = CInt(times[0])! * 3600
        var minutes: Int = CInt(times[1])! * 60
        var seconds: Int = CInt(times[2])!
        var time: Int = hours + minutes + seconds
            // Convert track duration time to seconds
        var durationString: String = positionInfoResponse["TrackDuration"]["text"]
        var durations: [AnyObject] = durationString.componentsSeparatedByString(":")
        var durationHours: Int = CInt(durations[0])! * 3600
        var durationMinutes: Int = CInt(durations[1])! * 60
        var durationSeconds: Int = CInt(durations[2])!
        var duration: Int = durationHours + durationMinutes + durationSeconds
        var queueIndex: Int = CInteger(positionInfoResponse["Track"]["text"])!
        var trackURI: String = positionInfoResponse["TrackURI"]["text"]
        var protocol: String = trackMetaData["DIDL-Lite"]["item"]["res"]["protocolInfo"]
        block(artist, title, album, albumArt, time, duration, queueIndex, trackURI, protocol, error)
    })
}

func () {
    var mediaInfo
}

func block() {
    self.upnp("/MediaRenderer/AVTransport/Control", soap_service: "urn:schemas-upnp-org:service:AVTransport:1", soap_action: "GetMediaInfo", soap_arguments: "<InstanceID>0</InstanceID>", completion: block)
}

func () {
    var playbackStatus
}

func block() {
    self.status({(response: [NSObject : AnyObject], error: NSError) -> Void in
        if block && error {
            block(false, nil, error)
        }
        if block != nil {
            var playState: String = response["CurrentTransportState"]
            if (playState == "PLAYING") {
                block(true, response, nil)
            }
            else if (playState == "PAUSED_PLAYBACK") || (playState == "STOPPED") {
                block(false, response, nil)
            }
        }
    })
}

func () {
    var status
}

func block() {
    self.upnp("/MediaRenderer/AVTransport/Control", soap_service: "urn:schemas-upnp-org:service:AVTransport:1", soap_action: "GetTransportInfo", soap_arguments: "<InstanceID>0</InstanceID>", completion: {(response: [NSObject : AnyObject], error: NSError) -> Void in
        if block != nil {
            if error != nil {
                block(nil, error)
            }
            var returnData: [NSObject : AnyObject] = ["CurrentTransportState": response["s:Envelope"]["s:Body"]["u:GetTransportInfoResponse"]["CurrentTransportState"]["text"]]
            block(returnData, nil)
        }
    })
}

func () {
    var browse
}

func block() {
    self.upnp("/MediaServer/ContentDirectory/Control", soap_service: "urn:schemas-upnp-org:service:ContentDirectory:1", soap_action: "Browse", soap_arguments: "<ObjectID>Q:0</ObjectID><BrowseFlag>BrowseDirectChildren</BrowseFlag><Filter>*</Filter><StartingIndex>0</StartingIndex><RequestedCount>0</RequestedCount><SortCriteria></SortCriteria>", completion: {(response: [NSObject : AnyObject], error: NSError) -> Void in
        if block != nil {
            if error != nil {
                block(nil, error)
            }
            var returnData: [NSObject : AnyObject] = [
                    "TotalMatches" : response["s:Envelope"]["s:Body"]["u:BrowseResponse"]["TotalMatches"]["text"]
                ]

            var queue: [NSObject : AnyObject] = XMLReader(dictionaryForXMLString: response["s:Envelope"]["s:Body"]["u:BrowseResponse"]["Result"]["text"], error: nil)
            NSLog("Queue: %@", queue)
            var queue_items: [AnyObject] = NSMutableArray()
            for queue_item: [NSObject : AnyObject] in queue["DIDL-Lite"]["item"] {
                // Spotify
                if (queue_item["res"]["protocolInfo"] == "sonos.com-spotify:*:audio/x-spotify:*") {
                    var item: [NSObject : AnyObject] = ["MetaDataCreator": queue_item["dc:creator"]["text"], "MetaDataTitle": queue_item["dc:title"]["text"], "MetaDataAlbum": queue_item["upnp:album"]["text"], "MetaDataAlbumArtURI": queue_item["upnp:albumArtURI"]["text"], "MetaDataTrackURI": queue_item["res"]["text"]]
                    queue_items.append(item)
                }
                // HTTP Streaming (SoundCloud?)
                if (queue_item["res"]["protocolInfo"] == "sonos.com-http:*:audio/mpeg:*") {
                    var item: [NSObject : AnyObject] = ["MetaDataCreator": queue_item["dc:creator"]["text"], "MetaDataTitle": queue_item["dc:title"]["text"], "MetaDataAlbum": "", "MetaDataAlbumArtURI": queue_item["upnp:albumArtURI"]["text"], "MetaDataTrackURI": queue_item["res"]["text"]]
                    queue_items.append(item)
                }
            }
            returnData["QueueItems"] = queue_items
            block(returnData, nil)
        }
    })
}