//
//  XMLReader.h
//
//  Created by Troy Brant on 9/18/10.
//  Updated by Antoine Marcadet on 9/23/11.
//  Updated by Divan Visagie on 2012-08-26
//
import Foundation
enum XMLReaderOptions : Int    // Specifies whether the receiver reports the namespace and the qualified name of an element.
 {
    case ProcessNamespaces
    // Specifies whether the receiver reports the scope of namespace declarations.
    case ReportNamespacePrefixes
    // Specifies whether the receiver reports declarations of external entities.
    case ResolveExternalEntities
}

class XMLReader: NSObject, NSXMLParserDelegate {
    class func dictionaryForXMLData(data: NSData, error: NSError) -> [NSObject : AnyObject] {
        var reader: XMLReader = XMLReader(error: error!)
        var rootDictionary: [NSObject : AnyObject] = reader.objectWithData(data!, options: 0)
        return rootDictionary
    }

    class func dictionaryForXMLString(string: String, error: NSError) -> [NSObject : AnyObject] {
        var data: NSData = string.dataUsingEncoding(NSUTF8StringEncoding)
        return XMLReader(dictionaryForXMLData: data, error: error!)
    }

    class func dictionaryForXMLData(data: NSData, options: XMLReaderOptions, error: NSError) -> [NSObject : AnyObject] {
        var reader: XMLReader = XMLReader(error: error!)
        var rootDictionary: [NSObject : AnyObject] = reader.objectWithData(data, options: options)
        return rootDictionary
    }

    class func dictionaryForXMLString(string: String, options: XMLReaderOptions, error: NSError) -> [NSObject : AnyObject] {
        var data: NSData = string.dataUsingEncoding(NSUTF8StringEncoding)
        return XMLReader(dictionaryForXMLData: data, options: options, error: error!)
    }
    var dictionaryStack: [AnyObject]
    var textInProgress: NSMutableString
    var errorPointer: NSError


    convenience override init(error: NSError) {
        if self.init() {
            if error! {
                self.errorPointer = error!
            }
        }
    }

    func objectWithData(data: NSData, options: XMLReaderOptions) -> [NSObject : AnyObject] {
        // Clear out any old data
        self.dictionaryStack = [AnyObject]()
        self.textInProgress = NSMutableString()
        // Initialize the stack with a fresh dictionary
        dictionaryStack.append(NSMutableDictionary())
            // Parse the XML
        var parser: NSXMLParser = NSXMLParser(data: data)
        parser.shouldProcessNamespaces = (options & .sProcessNamespaces)
        parser.shouldReportNamespacePrefixes = (options & .sReportNamespacePrefixes)
        parser.shouldResolveExternalEntities = (options & .sResolveExternalEntities)
        parser.delegate = self
        var success: Bool = parser.parse()
        // Return the stack's root dictionary on success
        if success {
            var resultDict: [NSObject : AnyObject] = dictionaryStack[0]
            return resultDict
        }
        return nil
    }

    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String, qualifiedName qName: String, attributes attributeDict: [NSObject : AnyObject]) {
            // Get the dictionary for the current level in the stack
        var parentDict: [NSObject : AnyObject] = dictionaryStack.lastObject()
            // Create the child dictionary for the new element, and initilaize it with the attributes
        var childDict: [NSObject : AnyObject] = NSMutableDictionary()
        childDict.addEntriesFromDictionary(attributeDict)
            // If there's already an item for this key, it means we need to create an array
        var existingValue: AnyObject = (parentDict[elementName] as! AnyObject)
        if existingValue != nil {
            var array: [AnyObject]? = nil
            if (existingValue is NSMutableArray.self) {
                // The array exists, so use it
                array = (existingValue as! [AnyObject])
            }
            else {
                // Create an array if it doesn't exist
                array = NSMutableArray()
                array!.append(existingValue)
                // Replace the child dictionary with an array of children dictionaries
                parentDict[elementName] = array!
            }
            // Add the new child dictionary to the array
            array!.append(childDict)
        }
        else {
            // No existing value, so update the dictionary
            parentDict[elementName] = childDict
        }
        // Update the stack
        dictionaryStack.append(childDict)
    }

    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String, qualifiedName qName: String) {
            // Update the parent dict with text info
        var dictInProgress: [NSObject : AnyObject] = dictionaryStack.lastObject()
        // Set the text property
        if textInProgress.characters.count > 0 {
                // trim after concatenating
            var trimmedString: String = textInProgress.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            dictInProgress[kXMLReaderTextNodeKey] = trimmedString.mutableCopy()
            // Reset the text
            self.textInProgress = NSMutableString()
        }
        // Pop the current dict
        dictionaryStack.removeLastObject()
    }

    func parser(parser: NSXMLParser, foundCharacters string: String) {
        // Build the text value
        textInProgress.appendString(string)
    }

    func parser(parser: NSXMLParser, parseErrorOccurred parseError: NSError) {
        // Set the error pointer to the parser's error object
        self.errorPointer = parseError
    }
}
//
//  XMLReader.m
//
//  Created by Troy Brant on 9/18/10.
//  Updated by Antoine Marcadet on 9/23/11.
//  Updated by Divan Visagie on 2012-08-26
//

let kXMLReaderTextNodeKey: String = "text"

let kXMLReaderAttributePrefix: String = "@"